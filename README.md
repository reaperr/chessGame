# chessGame
2D GUI chess game Python using Tkinter and PIL, event driven, OOP
<br />
<br />
IF YOU USE WINDOWS AND JUST WANT TO PLAY THE PURE GAME THEN DOWNLOAD THE 'DIST' FOLDER AND RUN THE EXE FILE. YOU MUST MAKE SURE YOU KEEP ITS 'MATS' FOLDER INSIDE IT OTHERWISE THE GAME WILL NOT WORK. Alternately just download it here with 1 button click https://fruffers.itch.io/chess
<br />
Or if you want to run the game in Python make sure you have PIL (pillow) and Tkinter modules.
<br />
<br />
*White moves first.
<br />
*It's a 2 player game. So grab a friend.
<br />
*Right click to deselect a piece and left click to pick another piece.
<br />
*Pawns that move to the final square at the other side of the board will randomly get promoted to either a Rook, Knight, Bishop or Queen. 
<br />
*Pawns can move 2 squares forward on only their first move.
<br />
*Pawns kill diagonally.
<br />
*Knights are the only pieces that can jump over their own coloured pieces.
<br />
*To win kill the other player's king.
<br />
*The king is the heart-shaped piece whilst the queen is the spikey piece.
<br />

![](https://github.com/fruffers/chessGame/blob/master/promote/game5.PNG)
![](https://github.com/fruffers/chessGame/blob/master/promote/game3.PNG)
![](https://github.com/fruffers/chessGame/blob/master/promote/game4.PNG)
